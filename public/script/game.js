

let playerOnTurn = 'X';
let inputListX = [];
let inputListO = [];
let result = 'unknown';


// determine the winner
function determineResult () { 
    const winner = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]];
    winner.map(win => {
        if (inputListX.sort().filter(item => win.includes(item)).length === 3) {
            result = 'X-wins';
        } else if (inputListO.sort().filter(item => win.includes(item)).length === 3) {
            result = 'O-wins';
        }
    });
}

// onclick function
function inputLetter (div, id) {
    //playing
    if (result === 'unknown' && div.innerText.length === 0){
        div.innerText = playerOnTurn;
        let index = parseInt(id.charAt(id.length - 1)); 

        // push index and switch playerOnTurn
        playerOnTurn === 'X' 
        ? (inputListX.push(index),playerOnTurn = 'O') 
        : (inputListO.push(index),playerOnTurn = 'X');

        determineResult();
        document.getElementById('hint').innerText = 'Next round: ' + playerOnTurn;
    } 
    // output result
    if (result === 'X-wins') {
        document.getElementById('result').innerText = 'Congratulations, X wins!';
    } else if (result === 'O-wins') {
        document.getElementById('result').innerText = 'Congratulations, O wins!';
    } else if (inputListX.length + inputListO.length === 9){
        document.getElementById('result').innerText = 'The game ends as a tie.';
    }
}


